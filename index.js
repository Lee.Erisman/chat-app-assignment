const { Console } = require('console');
const { Socket } = require('dgram');

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var UsernameArray = ["Bulbasaur","Ivysaur","Venusaur","Charmander","Charmeleon","Charizard","Squirtle","Wartortle","Blastoise","Caterpie","Metapod","Butterfree","Weedle","Kakuna","Beedrill","Pidgey","Pidgeotto","Pidgeot","Rattata","Raticate","Spearow","Fearow","Ekans","Arbok","Pikachu","Raichu","Sandshrew","Sandslash","Nidoran","Nidorina","Nidoqueen","Nidoran","Nidorino","Nidoking","Clefairy","Clefable","Vulpix","Ninetales","Jigglypuff","Wigglytuff","Zubat","Golbat","Oddish","Gloom","Vileplume","Paras","Parasect","Venonat","Venomoth","Diglett","Dugtrio","Meowth","Persian","Psyduck","Golduck","Mankey","Primeape","Growlithe","Arcanine","Poliwag","Poliwhirl","Poliwrath","Abra","Kadabra","Alakazam","Machop","Machoke","Machamp","Bellsprout","Weepinbell","Victreebel","Tentacool","Tentacruel","Geodude","Graveler","Golem","Ponyta","Rapidash","Slowpoke","Slowbro","Magnemite","Magneton","Farfetch'd","Doduo","Dodrio","Seel","Dewgong","Grimer","Muk","Shellder","Cloyster","Gastly","Haunter","Gengar","Onix","Drowzee","Hypno","Krabby","Kingler","Voltorb","Electrode","Exeggcute","Exeggutor","Cubone","Marowak","Hitmonlee","Hitmonchan","Lickitung","Koffing","Weezing","Rhyhorn","Rhydon","Chansey","Tangela","Kangaskhan","Horsea","Seadra","Goldeen","Seaking","Staryu","Starmie","Mr. Mime","Scyther","Jynx","Electabuzz","Magmar","Pinsir","Tauros","Magikarp","Gyarados","Lapras","Ditto","Eevee","Vaporeon","Jolteon","Flareon","Porygon","Omanyte","Omastar","Kabuto","Kabutops","Aerodactyl","Snorlax","Articuno","Zapdos","Moltres","Dratini","Dragonair","Dragonite","Mewtwo","Mew"];
var takenUsernames = [];
var messageHistory = [];
var messageColor = [];
var usernamesInUse = [];
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

//unique message handling
io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    messageType = handleMessages(msg);    
    
    if(messageType[0]=="normalMessage"){
      msg = ReturnPrettyTimestamp() + " : " + socket.username + " : " + msg;
      messageHistory.push(msg);
      messageColor.push(socket.color);
      socket.broadcast.emit('chat message', {msg:msg, userColor: socket.color});
      io.sockets.connected[socket.id].emit('local chat message', {msg:msg, userColor: socket.color});
    }
    else if(messageType[0]=="colourChange"){
      socket.color = "#" + messageType[1];
      var ChosenColour = messageType[1];
      io.sockets.connected[socket.id].emit('colour change', ChosenColour);
    }
    else if(messageType[0]=="nameChange"){
      var oldName = socket.username;
      socket.username = messageType[1];
      var ChosenName = messageType[1];
      var removeAt;
      if(!usernamesInUse.includes(ChosenName)){
        io.sockets.connected[socket.id].emit('username change', {ChosenName:ChosenName, userColor: socket.color});
        for (var i = 0; i < usernamesInUse.length+1; i++) {         
          if(usernamesInUse[i]==oldName){
            //time to remove the old name
            removeAt=i;
          }
        }
        usernamesInUse.splice(removeAt, 1);
        //allow username
        sessionUsernameIndex = UsernameArray.length;
        UsernameArray.push(ChosenName);
        takenUsernames.push(sessionUsernameIndex);
        usernamesInUse.push(UsernameArray[sessionUsernameIndex]);
        io.emit('newUserConnection', usernamesInUse);
      }
      else{
        //already taken
        socket.username = oldName;
        io.sockets.connected[socket.id].emit('username change fail', {ChosenName:ChosenName, userColor: socket.color});
      }

           
    }
  });
});

//user connection handling
io.on('connection', (socket) => {
  socket.color = "#000000";
  sessionUsernameIndex = getAvailableUsernameIndex();
  let freeUsernameFound = false;
  //Iterate until we find a free username
  while(!freeUsernameFound){

    if (!takenUsernames.includes(sessionUsernameIndex)){
      takenUsernames.push(sessionUsernameIndex);
      
      socket.username = UsernameArray[sessionUsernameIndex];
      usernamesInUse.push(UsernameArray[sessionUsernameIndex]);
      let UserJoined = socket.username;
      let LocalUsername = socket.username;
      io.sockets.connected[socket.id].emit('newLocalUser', {Local: LocalUsername, messageHist: messageHistory, setColor: messageColor});
      io.emit('newUser', UserJoined);
      io.emit('newUserConnection', usernamesInUse);
      freeUsernameFound = true;

    }
    else{
      sessionUsernameIndex = getAvailableUsernameIndex();
    }
  }
  console.log(socket.username + ' connected');

  //user disconnect handling
  socket.on('disconnect', () =>{
    console.log(socket.username + ' disconnected');
    const index = takenUsernames.indexOf(sessionUsernameIndex);
    if (index > -1) {
      takenUsernames.splice(index, 1);
    }
    const index2 = usernamesInUse.indexOf(socket.username);
    if (index2 > -1) {
      usernamesInUse.splice(index2, 1);
    }
    let UserLeft = socket.username;
    io.emit('removeUser', UserLeft);
    io.emit('removeUserConnection', usernamesInUse);
  });
});

http.listen(port, function(){
  console.log('listening on *:' + port);
});


function ReturnPrettyTimestamp(){
  let DateUgly = new Date();

  // current date
  // slice may remove 0 if not required so that single digit dates display with leading zero but double digit ones don't
  let date = ("0" + DateUgly.getDate()).slice(-2);

  // current year
  let year = DateUgly.getFullYear();
  
  // current month
  let month = ("0" + (DateUgly.getMonth() + 1)).slice(-2);

  // current hours
  let hours = DateUgly.getHours();

  // current minutes
  let minutes = DateUgly.getMinutes();

  let stamp = year + "-" + month + "-" + date + " " + hours + ":" + minutes;

  return stamp;
}

function getAvailableUsernameIndex(){

  let randomIndex = Math.floor(Math.random()*UsernameArray.length);

  return randomIndex
}
function handleMessages(message){

  if (message.includes("/color") && message.length==13){
    //allow for colour change command
    //grab substring with RRGGBB
    try{
      Result = message.substring(7);
      return ["colourChange" , Result]
    }
    catch{
      return ["normalMessage", message]
    }

  }
  else if(message.startsWith("/name")){
    //allow for name change command
    //grab all text after "/name "
    try{
      Result = message.substring(6);
      return ["nameChange" , Result]
    }
    catch{
      return ["normalMessage", message]
    }
  }
  else{
    return ["normalMessage", message]
  }
}